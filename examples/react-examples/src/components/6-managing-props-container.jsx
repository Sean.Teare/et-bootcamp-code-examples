import { useState } from 'react';
import PropsTemplate from './6-managing-props';

const ManagingPropsContainer = ({ textContent = '' }) => {
  const availableColors = ['red', 'green', 'blue', 'orange', 'purple'];
  const [currentColorIndex, setCurrentColorIndex] = useState(0);

  const changeColor = () => {
    setCurrentColorIndex(currentColorIndex + 1);
  }

  return (
    <div>
        <h1>Next Color: <span style={{ color: availableColors[(currentColorIndex + 1) % availableColors.length]}}>{availableColors[(currentColorIndex + 1) % availableColors.length]}</span></h1>
        <PropsTemplate textContent={textContent} textColor={availableColors[currentColorIndex % availableColors.length]} onChangeColorClick={changeColor} />;

    </div>
  )
  
}

export default ManagingPropsContainer;