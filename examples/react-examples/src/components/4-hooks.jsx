import { useState, useEffect } from 'react';

const HookComponent = () => {
  const [timerOff, setTimerOff] = useState(false);
 
  useEffect(() => {
    console.log('component is mounting');
    const timer = setTimeout(() => {
      setTimerOff(true);
    }, 5000);

    return () => {
      clearTimeout(timer);
    }
  }, []);

  useEffect(() => {
    if (timerOff) {
      console.log('my timer went off');
    }
  }, [timerOff])

  return (
    <div>
      <div>Hook Component</div>
      <div>{timerOff ? 'timer has gone off' : 'waiting for timer'}</div>
    </div>
  )
}

export default HookComponent;