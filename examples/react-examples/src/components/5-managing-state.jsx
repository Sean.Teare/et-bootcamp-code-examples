import { useState } from 'react';

const StateManagementComponent = () => {
  let [counter, setCounter] = useState(0);
  
  const incrementCounter = () => {
    setCounter(counter + 1);
  }

  return (
    <div>
      <div>State Management Component</div>
      <div>
        <span>Counter: </span><span>{counter}</span>
      </div>
      <div>
        <button onClick={incrementCounter}>Increment Count</button>
      </div>
    </div>
  );
};

export default StateManagementComponent;