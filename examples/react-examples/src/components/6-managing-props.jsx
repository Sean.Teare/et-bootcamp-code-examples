const PropManagementComponent = ({ textContent = 'content', textColor = 'black', onChangeColorClick = () => {} }) => {
  return (
    <div>
      <div>State Management Component</div>
      <div style={{ color: textColor }}>{textContent}</div>
      <button onClick={onChangeColorClick}>Change Color</button>
    </div>
  )
};

export default PropManagementComponent;