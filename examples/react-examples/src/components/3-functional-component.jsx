const FunctionalComponent = (props) => {
  return (
    <div>
      <div>{props.title}</div>
      <div>Functional Component</div>
    </div>
  )
}

export default FunctionalComponent;