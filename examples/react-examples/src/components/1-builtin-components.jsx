const BuiltIn = () => {
  const onInputChange = (e) => {
    console.log('changing', e.target.name, e.target.value);
  }

  const onFormSubmit = (e) => {
    e.preventDefault();
    console.log('Submitting form');
  }
  const onButtonClick = () => {
    console.log('clicking button 2');
  }

  const handleSelectChange = (e) => {
    console.log('Changing select', e.target.value);
  }
  return (
    <div >
      <h1>Header</h1>
      <div>Div</div>
      <img alt="icon" src="https://www.ally.com/content/dam/assets/ally-assets/images/osa_money_home.png" />
      <form onSubmit={onFormSubmit}>
        <input onChange={onInputChange} name="input1" type="text" />
        <select onChange={handleSelectChange}>
          <option value="1">Option1</option>
          <option value="2">Option2</option>
        </select>
        <input type="submit" value="Submit" />
        
      </form>
      <button onClick={onButtonClick}>Button 2</button>
    </div>
  );
}

export default BuiltIn;