import { useState, useMemo } from 'react';
import axios from 'axios';

const FormInputComponent = () => {
  const [searchTerm, setSearchTerm] = useState('');
  const [resultsSearchTerm, setResultsSearchTerm] = useState('');
  const [searchResults, setSearchResults] = useState([]);
  const [selectedId, setSelectedId] = useState('');
  
  const submitForm = async (e) => {
    e.preventDefault();
    if (searchTerm.toLowerCase() !== resultsSearchTerm.toLowerCase()) {
      setResultsSearchTerm(searchTerm);
      const { data }  = await axios.get(`http://www.omdbapi.com?apikey=6617844d&s=${searchTerm}`)
      setSearchResults(data.Search)
      setSelectedId('')
    }
  };

  const onSearchTermChange = ({ target: { value } }) => {
    setSearchTerm(value);
  }; 
  const changeSelectedId = (e) =>  {
    setSelectedId(e.target.value);
  }

  const displayResults = useMemo(() => {
    if (!selectedId) return searchResults;
    const foundResult = searchResults.find(res => res.imdbID === selectedId);
    return !foundResult ? searchResults : [foundResult];
  }, [selectedId, searchResults]);

  return (
    <div>
      <h1>Form Input Component</h1>
      <form onSubmit={(e) => submitForm(e)}>
        <div>
          {searchResults.length > 0 ? (
            <select name="selectedId" value={selectedId} onChange={changeSelectedId}>
              <option value=''>Select Option</option>
              {searchResults.map(res => (
                <option value={res.imdbID}>{res.Title}</option>
              ))}
            </select> 
          ) : null}
        </div>
        <div>
          <input type="text" value={searchTerm} onChange={onSearchTermChange}/>
          <input type="submit" value="Submit Form"/>
        </div>
      </form>
      {displayResults.length > 0 ? (
      <ul>
        {displayResults.map(movie => (
          <li key={movie.imdbID}>
            <div>Title: {movie.Title}</div>
            <div>Year: {movie.Year}</div>
          </li>
        ))}
      </ul>
    ) : null}
    </div>
  );
};

export default FormInputComponent;