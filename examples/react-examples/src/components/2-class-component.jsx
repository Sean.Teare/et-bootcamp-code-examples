import React from 'react';

class ClassComponent extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      date: new Date()
    }
  }
  // perform initialization actions when the component is loaded
  componentDidMount() {
    this.timer = setInterval(() => console.log('tick'), 1000);
  }

  // perform teardown actions when the component is destroyed
  componentWillUnmount() {
    clearInterval(this.timer);
  }
  render() {
    return (
      <div>
        <div>{this.props.title}</div>
        <div>Timer Component</div>
      </div>
    )
  }
}

export default ClassComponent;
