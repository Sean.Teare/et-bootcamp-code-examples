// import BuiltIn from './components/1-builtin-components';
// import ClassComponent from './components/2-class-component';
// import FunctionalComponent from './components/3-functional-component';
// import HookComponent from './components/4-hooks';
// import StateManagementComponent from './components/5-managing-state';
// import PropManagementComponent from './components/6-managing-props-container';
import FormInputComponent from './components/7-form-input';

function App() {
  return (
    <div className="App">
      {/* <BuiltIn /> */}
      {/* <ClassComponent title="My Title from Props" /> */}
      {/* <FunctionalComponent title="My Title from Props Also" /> */}
      {/* <HookComponent /> */}
      {/* <StateManagementComponent /> */}
      {/* <PropManagementComponent textContent="My Color Changing Container" /> */}
      <FormInputComponent />
    </div>
  );
}

export default App;
