const printingPromise = () => {
  return new Promise((resolve) => {
    resolve('printing promise executing');
  })
};

const erroringPromise = (shouldThrow = false) => {
 return new Promise((resolve, reject) => {
    if (shouldThrow) {
      reject(new Error('error promise throwing'));
    }
    resolve('no error')
  })
}

printingPromise().then((result) => {
  console.log('executing then from printingPromise', result);
}).catch((err) => {
  console.log('Error in printingPromise');
});

erroringPromise(true).then((result) => {
  console.log('executing then from erroringPromise', result);
}).catch((err) => {
  console.log('Error in erroringPromise');
});

console.log('executing synchronously after promises');

// const printingAsyncFunc = async () => {
//   return 'printing async func executing';
// }

// const erroringAsyncFunc = () => {
//   throw new Error('Error in erroringAsyncFunc');
// }

// (async () => {
//   try {
//     console.log(await printingAsyncFunc());
//     console.log(await erroringAsyncFunc());
//   } catch (err) {
//     console.log('caught error', err)
//   }
// })()

