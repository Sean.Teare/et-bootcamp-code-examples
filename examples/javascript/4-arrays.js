const myArr = [];
const myNonEmptyArr = ['test', 1, 'anotherValue', ['anotherArray'], { key1: 'val1'}];

console.log('myArry', myArr);
console.log('myNonEmptyArr', myNonEmptyArr);


// adding and removing values from array

// const myArr = [];
// myArr.push({ id: 1, name: 'one' });
// myArr.push({ id: 2, name: 'two' });
// myArr.push({ id: 3, name: 'three' });

// console.log('myArr', myArr);
// myArr.pop();
// console.log('myArr after pop', myArr);
// myArr.shift();
// console.log('myArr after shift', myArr);



// iterating through array

// const myArr = [];
// myArr.push({ id: 1, name: 'one' });
// myArr.push({ id: 2, name: 'two' });
// myArr.push({ id: 3, name: 'three' });

// myArr.forEach((element, index) => {
//   console.log('index ' + index, element);
// })

// const myArrLength = myArr.length;
// for(let i = 0; i < myArrLength; i++) {
//   console.log('index ' + i, myArr[i]);
// }


// sorting array
//
// const myArr = [];
// myArr.push({ id: 1, name: 'one' });
// myArr.push({ id: 2, name: 'two' });
// myArr.push({ id: 3, name: 'three' });

// myArr.sort((curr, next) => {
//   if (curr.id < next.id) {
//     return 1;
//   } else {
//     return -1;
//   }
// });

// console.log('my sorted array', myArr);


// array mapping

// const myArr = [];
// myArr.push({ id: 1, name: 'one' });
// myArr.push({ id: 2, name: 'two' });
// myArr.push({ id: 3, name: 'three' });

// const newArr = myArr.map((element) => {
//   return element.name;
// });

// console.log('my original array', myArr);
// console.log('my mapped array', newArr);
