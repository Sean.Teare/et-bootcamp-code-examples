const isTrue = true;

if (isTrue) {
  console.log('i am true');
} else {
  console.log('i am false');
}

// let loopEntered = false;

// while (!loopEntered) {
//    console.log('entered?', loopEntered)
//   loopEntered = true;
  
// }



// loopEntered = false;
// do {
//   console.log('entered?', loopEntered);
//   loopEntered = true;
// } while (!loopEntered)



// const condition = 'switchtype';

// switch (condition) {
//   case 'switchtype':
//     console.log('condition is switchtype');
//     break;
//   case 'switchtype2':
//     console.log('condition is switchtype2');
//     break;
//   default:
//     console.log('condition has no matches');
// }
 


// const errFunc = (throwErr = true) => {
//   if (throwErr) {
//     throw new Error('I have an error');
//   }
// }

// try {
//   // errFunc();
//   errFunc(false);
//   console.log('no error found');
// } catch (err) {
//   console.log('i have an error', err);
// } finally {
//   console.log('i am finally here');
// }

