const myObject = {};
const myNonEmptyObject = {
  initialKey: 'initialKey'
};

console.log('my object', myObject);
console.log('myNonEmptyObject', myNonEmptyObject);

// const myObject = {};

// myObject.newKey = 'newKey';
// myObject.newArray = ['test', 'this'];
// myObject.newObject = {
//   newObj1: 'newObj1',
//   newObj2: 'newObj2',
//   nestedObj: {
//     nestedObjKey: true
//   }
// };
// console.log('myObject', myObject);