const globalLet = 'globalLet';
const globalConst = 'globalConst';
const globalObj = {};
const globalNum = 1;
const myFunc = (printGlobal = false) => {
  const localConst = 'localConst';
  let localLet = 'localLet'
  console.log('inside myfunc', localLet);
  console.log('inside myfunc', localConst);

  if (printGlobal) {
    console.log('insidemy func', globalLet);
    console.log('inside myfunc', globalConst);
  }
}

myFunc();

// myFunc(true);

// console.log('global', globalLet);
// console.log('global', globalConst),

// console.log('global', localLet);
// console.log('global', localConst);

// globalObj.newProp = 1;
// console.log('global', globalObj);

// globalNumb = 2;