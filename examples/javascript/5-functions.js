// // two styles of creating functions

const arrowFunction = (name) => {
  return 'my name is ' + name;
}

function conventionalFunc(name) {
  return 'my name is ' + name;
}

console.log(arrowFunction('arrowFunction'));
console.log(conventionalFunc('conventionalFunction'));


// function passed into a function as a param
// const decoratorFunction = (func = () => {}) => (name) => {
//   console.log('executing function', name);
//   func();
// }

// const myFunc = () => {
//   console.log('executing my param function');
// }
// const decoratedFunction = decoratorFunction(myFunc);
// console.log('executing my decoratedFunction', decoratedFunction());


// function returning a function
// const generatePrintFunc = (number) => {
//   return () => {
//     console.log('printing ' + number);
//   }
// }

// const print1 = generatePrintFunc(1);
// const print2 = generatePrintFunc(2);

// print1();
// print2();

