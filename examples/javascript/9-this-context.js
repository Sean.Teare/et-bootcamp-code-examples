// put this code in the browser to demonstrate;

console.log('global this', this);

function functionalThis() {
  console.log('functionalThis this', this);
}

console.log(functionalThis());

const arrowFunctionThis = () => {
  console.log('arrowFunction this', this);
}

console.log(arrowFunctionThis());
const boundArrowFunc = arrowFunctionThis.bind(this);

const obj = {
  arrowThis: arrowFunctionThis,
  arrowBound: boundArrowFunc,
  arrowThisInObj:  () => {
    console.log('arrowThisInObj', this);
  }
}

console.log('arrow this', obj.arrowThis());
console.log('arrow bound', obj.arrowBound());
console.log('arrow in obj', obj.arrowThisInObj());
// const person = {
//   firstName: "John",
//   lastName : "Doe",
//   id       : 1,
//   fullName : function() {
//     return this.firstName + " " + this.lastName;
//   }
// };

// console.log('persons name', person.fullName());

// const person2 = {
//   firstName: "Jane",
//   lastName : "Doe",
//   id       : 2,
// };

// const person2FullName = person.fullName.bind(person2);

// console.log('person2Fullname', person2FullName());